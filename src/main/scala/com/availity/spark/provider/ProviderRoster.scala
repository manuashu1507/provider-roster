import org.apache.spark.sql.{SparkSession, DataFrame}
import org.apache.spark.sql.functions._
import org.slf4j.LoggerFactory

object ProviderVisitCountPerMonth {
  val logger = LoggerFactory.getLogger(ProviderVisitCountPerMonth.getClass)

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("Provider Visit Count Per Month")
      .getOrCreate()

    logger.info("Spark session created")

    import spark.implicits._

    try {
      val providersPath = "path/to/providers.csv"
      val visitsPath = "path/to/visits.csv"
      val outputPath = "path/to/output/provider_visits_per_month"

      val providersDF = loadCSV(spark, providersPath)
      val visitsDF = loadCSV(spark, visitsPath)

      val visitsWithMonthDF = addVisitMonth(visitsDF)

      val visitCountsDF = countVisitsPerProviderPerMonth(visitsWithMonthDF, providersDF)

      saveAsJson(visitCountsDF, outputPath)
    } catch {
      case e: Exception =>
        logger.error("An error occurred: ", e)
    } finally {
      spark.stop()
      logger.info("Spark session stopped")
    }
  }

  def loadCSV(spark: SparkSession, path: String): DataFrame = {
    logger.info(s"Loading CSV from path: $path")
    spark.read.option("header", "true").csv(path)
  }

  def addVisitMonth(visitsDF: DataFrame): DataFrame = {
    logger.info("Adding month column to visits DataFrame")
    visitsDF.withColumn("month", date_format($"visit_date", "yyyy-MM"))
  }

  def countVisitsPerProviderPerMonth(visitsDF: DataFrame, providersDF: DataFrame): DataFrame = {
    logger.info("Joining DataFrames and counting visits per provider per month")
    val joinedDF = visitsDF.join(providersDF, "provider_id")
    joinedDF.cache() // Cache the joined DataFrame to optimize performance

    val visitCountsDF = joinedDF.groupBy("provider_id", "month")
      .agg(count("visit_id").alias("num_visits"))

    visitCountsDF
  }

  def saveAsJson(df: DataFrame, outputPath: String): Unit = {
    logger.info(s"Saving result as JSON to path: $outputPath")
    df.coalesce(1) // Coalesce to a single partition to reduce the number of output files
      .write.json(outputPath)
  }
}
